/**
 * @file
 * Global utilities.
 *
 */
(function ($, Drupal) {

    'use strict';

    Drupal.behaviors.senapi_ui = {
        attach: function (context, settings) {



            if (!context.activeElement) {
                return false;
            }

             $(window).on('load',function(){
                $('#myModal').modal('show');
            });

            $('.producciones-carousel').owlCarousel({
                loop: true,
                margin: 10,
                //nav: true,
                autoplay: true,

                center: true,
                autoplaySpeed: 2000,
                autoplayTimeout: 5000,
                autoplayHoverPause: true,
                animateIn: 'fadeIn',
                animateOut: 'fadeOut',
                transitionStyle: 'fade',
                stagePadding: 0,
                mouseDrag: true,
                touchDrag: true,
                dots: true,
                dotClass: 'owl-dot',
                dotsClass: 'owl-dots',
                items: 1,
                autoWidth:'auto',
                mobileFirst: true,
                responsive: {
                    0:{
                        items:1
                    },
                    400:{
                        items:2
                    },
                    700:{
                        items:3
                    },
                    1000:{
                        items:5,
                        margin: 20
                    }
                }
            });



            pdfjsLib.GlobalWorkerOptions.workerSrc = settings.senapi_ui.pdfWorkerSrc;

            function isCanvasSupported() {
                var elem = document.createElement('canvas');
                return !!(elem.getContext && elem.getContext('2d'));
            }

            console.log(context);

            var hide = false;

            $('#bd-modal-lg').on('show.bs.modal', function (event) {
                event.stopPropagation();
                console.log(event);
                var element = $(event.relatedTarget);
                var url = element.data('url');
                if (!url) {
                    url = element.attr('href');
                }

                var modal = $(this);

                modal.append('<div id="control-icon" class="position-fixed fixed-top"><div class="btn-group-vertical">' +
                    '<button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>\n' +
                    '<button id="zoom-next" type="button" class="btn btn-secondary"><i class="fa fa-search-plus" aria-hidden="true"></i></button>\n' +
                    '<button id="zoom-prev" type="button" class="btn btn-secondary"><i class="fa fa-search-minus" aria-hidden="true"></i></button>\n' +
                    '<button id="download" type="button" class="btn btn-secondary"><i class="fa fa-external-link" aria-hidden="true"></i></button>\n' +
                    '</div></div>');

                var classes = ['modal-lg', 'modal-lg-70', 'modal-lg-80', 'modal-lg-90', 'modal-lg-100'];
                var currentClass = 1;

                var dialog = modal.find('.modal-dialog');
                for (var i = 0; i < classes.length; i++) {
                    if ($(dialog).hasClass(classes[i])) {
                        currentClass = i + 1;
                        break;
                    }
                }

                modal.find('#zoom-next').on('click', function (e) {
                    e.stopPropagation();

                    if (currentClass < classes.length) {
                        $(dialog).removeClass().addClass('modal-dialog ' + classes[currentClass++]);
                    }
                });

                modal.find('#zoom-prev').on('click', function (e) {
                    e.stopPropagation();

                    if ((currentClass - 2) >= 0) {
                        $(dialog).removeClass().addClass('modal-dialog ' + classes[(currentClass - 2)]);
                        currentClass--;
                        if (currentClass < 0) {
                            currentClass = 0;
                        }
                    }
                });

                modal.find('#download').on('click', function (e) {
                    e.stopPropagation();
                    var a = document.createElement('a');
                    a.target="_blank";
                    a.href=url;
                    a.click();
                });

                var content = modal.find('.modal-content');
                content.html('');

                var loading = $('<div style="height: 500px;background: white; position: relative;"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>');
                content.append(loading);

                if (isCanvasSupported()) {

                    var currentPage = 1;

                    pdfjsLib.getDocument(url).then(function (pdf) {
                        hide = false;
                        if (currentPage <= pdf.numPages) getPage();

                        function getPage() {

                            if (hide) {
                                currentPage = pdf.numPages;
                                content.html();
                                return;
                            }

                            pdf.getPage(currentPage).then(function (page) {
                                var scale = 1.5;
                                var viewport = page.getViewport(scale);

                                var canvas = document.createElement('canvas');
                                var ctx = canvas.getContext('2d');

                                canvas.height = viewport.height;
                                canvas.width = viewport.width;

                                canvas.classList.add('w-100');
                                canvas.classList.add('h-100');

                                var renderContext = {
                                    canvasContext: ctx,
                                    viewport: viewport
                                };

                                page.render(renderContext).then(function () {
                                    $(loading).remove();
                                    /*var img = new Image;
                                     img.onload = function () {
                                        ctx.drawImage(this, 0,0, ctx.canvas.width, ctx.canvas.height);
                                     };
                                     img.src = _canvas.toDataURL();
                                     img.classList.add('w-100');
                                     img.classList.add('h-100');
                                     $(content).append(img);*/

                                    $(content).append(canvas);

                                    if (currentPage < pdf.numPages) {
                                        currentPage++;
                                        getPage();
                                    }
                                });
                            });
                        }
                    });
                }
            });

            $('#bd-modal-lg').on('hidden.bs.modal', function (event) {
                var modal = $(this);
                var content = modal.find('.modal-content');
                modal.find('#control-icon').remove();

                content.html('');

                hide = true;
            });

        }
    };

})(jQuery, Drupal);
