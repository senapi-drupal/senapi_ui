<?php

/**
 * @file
 * Functions to support theming in the senapi_ui theme.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

function senapi_ui_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $theme = \Drupal::theme()->getActiveTheme()->getName();
  $form['settings'] = [
    '#type' => 'details',
    '#title' => t('Theme settings'),
    '#open' => TRUE,
  ];

  // Header settings
  $form['settings']['header'] = [
    '#type' => 'details',
    '#title' => t('Header settings'),
    '#open' => FALSE,
  ];

  $form['settings']['header']['header_layout'] = [
    '#type' => 'select',
    '#title' => t('Header default sidebar'),
    '#options' => [
      'default' => t('Default'),
      'header2' => t('Header 2'),
      'header3' => t('Header 3'),
    ],
    '#required' => TRUE,
    '#default_value' => theme_get_setting('header_layout', $theme),
  ];

  // Footer settings
  $form['settings']['footer'] = [
    '#type' => 'details',
    '#title' => t('Footer settings'),
    '#open' => FALSE,
  ];

  $form['settings']['footer']['footer_layout'] = [
    '#type' => 'select',
    '#title' => t('Footer default sidebar'),
    '#options' => [
      'default' => t('Default'),
      'footer2' => t('Footer 2'),
      'footer3' => t('Footer 3'),
    ],
    '#required' => TRUE,
    '#default_value' => theme_get_setting('footer_layout', $theme),
  ];

  //Page settings
  $form['settings']['page_title'] = [
    '#type' => 'details',
    '#title' => t('Page title settings'),
    '#open' => FALSE,
  ];

  $form['settings']['page_title']['page_title_layout'] = [
    '#type' => 'select',
    '#title' => t('Page title settings'),
    '#options' => [
      'page_title1' => t('Layout: Page title 1'),
      'page_title2' => t('Layout: Page title 2'),
    ],
    '#required' => TRUE,
    '#default_value' => theme_get_setting('page_title_layout', $theme),
  ];

  //Blog settings
  $form['settings']['blog'] = [
    '#type' => 'details',
    '#title' => t('Blog settings'),
    '#open' => FALSE,
  ];

  $form['settings']['blog']['blog_layout'] = [
    '#type' => 'select',
    '#title' => t('Blog layout'),
    '#options' => [
      'classic' => t('Classic layout'),
      'default' => t('Default layout'),
      'thumbnail' => t('Thumbnail layout'),
      'one_column' => t('One column grid'),
      'two_column' => t('Two column grid'),
      'three_column' => t('Three column grid'),
      'four_column' => t('Four column grid'),
    ],
    '#required' => TRUE,
    '#default_value' => theme_get_setting('blog_layout', $theme),
  ];

  $form['settings']['blog']['blog_sidebar'] = [
    '#type' => 'select',
    '#title' => t('Blog list default sidebar'),
    '#options' => [
      'left' => t('Left'),
      'right' => t('Right'),
      'full_width' => t('Full width'),
    ],
    '#required' => TRUE,
    '#default_value' => theme_get_setting('blog_sidebar', $theme),
  ];
}

/**
 * Implements hook_preprocess_HOOK() for html.html.twig.
 */
function senapi_ui_preprocess_html(array &$variables) {

}

/**
 * Implements hook_preprocess_page() for page.html.twig.
 */
function senapi_ui_preprocess_page(array &$variables) {
  $variables['#attached']['library'][] = 'senapi_ui/sidr';
  $variables['#attached']['drupalSettings'] = [
    'senapi_ui' => [
      'pdfWorkerSrc' => file_create_url(base_path() . 'libraries/pdf.js/build/pdf.worker.js'),
    ],
  ];

  if ($variables['is_front']) {
    // pagina principal
  }
}

/**
 * Implements hook_preprocess_HOOK() for Block document templates.
 */
function senapi_ui_preprocess_block(array &$variables) {
   switch ($variables['base_plugin_id']) {
    case 'system_branding_block':
      if ($variables['content']['site_logo']['#access'] && $variables['content']['site_logo']['#uri']) {
        $variables['site_logo'] = str_replace('.svg', '.png', $variables['content']['site_logo']['#uri']);
      }
      break;
  }
}

/**
 * Implements hook_preprocess_node().
 */
function senapi_ui_preprocess_node(array &$variables) {

}

/**
 * Implement hook_preprocess_region()
 */
function senapi_ui_preprocess_region(&$variables) {
    $nowrap = [
    'pre_header',
    'header',
    'highlighted',
    'tabs',
    'banner_top',
    'breadcrumbs',
    'page_title',
    'content',
    'sidebar',
    'content_bottom',
    'footer',
    'bottom',
  ];

  if(in_array($variables['elements']['#region'], $nowrap)) {
    $variables['attributes']['class'][] = 'row';
    $variables['attributes']['class'][] = 'no-padding';
  }

}

/**
 * Implements hook_form_search_block_form_alter()
 */
function senapi_ui_form_search_block_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {

}

/**
 * Implements hook_preprocess_breadcrumb()
 */
function senapi_ui_preprocess_breadcrumb(&$variables) {

}

/**
 * hook_preprocess()
 */
function senapi_ui_preprocess(&$variables) {

}


/**
 * Implements hook_page_attachments_alter().
 */
function senapi_ui_page_attachments_alter(array &$page) {

}


/**
 * Implements hook_theme_suggestions_page_alter().
 */
function senapi_ui_theme_suggestions_page_alter(array &$suggestions, array $variables) {

}

/**
 * Implements hook_theme_suggestions_node_alter().
 */
function senapi_ui_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  /*$node = $variables['elements']['#node'];

  if ($variables['elements']['#view_mode'] == "full") {

  }*/
}

/**
 * Implements hook_theme_suggestions_field_alter().
 */
function senapi_ui_theme_suggestions_field_alter(array &$suggestions, array $variables) {
  /*$element = $variables['element'];
  $suggestions[] = 'field__' . $element['#view_mode'];
  $suggestions[] = 'field__' . $element['#view_mode'] . '__' . $element['#field_name'];*/
}

/**
 * Implements hook_theme_suggestions_field_alter().
 */
function senapi_ui_theme_suggestions_fieldset_alter(array &$suggestions, array $variables) {
  /*$element = $variables['element'];
  if (isset($element['#attributes']['class']) && in_array('form-composite', $element['#attributes']['class'])) {
    $suggestions[] = 'fieldset__form_composite';
  }*/
}


/**
 * Implements hook_theme_suggestions_views_view_alter().
 */
function senapi_ui_theme_suggestions_views_view_alter(array &$suggestions, array $variables) {

}

/**
 * Implements hook_preprocess_form().
 */
function senapi_ui_preprocess_form(array &$variables) {
  //$variables['attributes']['novalidate'] = 'novalidate';
}

/**
 * Implements hook_preprocess_select().
 */
function senapi_ui_preprocess_select(array &$variables) {
  //$variables['attributes']['class'][] = 'select-chosen';
}

/**
 * Implements hook_preprocess_field().
 */
function senapi_ui_preprocess_field(array &$variables, $hook) {
  /*switch ($variables['element']['#field_name']) {
  }*/
}

/**
 * Implements hook_preprocess_details().
 */
function senapi_ui_preprocess_details(array &$variables) {
  /*$variables['attributes']['class'][] = 'details';
  $variables['summary_attributes']['class'] = 'summary';*/
}

/**
 * Implements hook_theme_suggestions_details_alter().
 */
function senapi_ui_theme_suggestions_details_alter(array &$suggestions, array $variables) {

}

/**
 * Implements hook_preprocess_menu_local_task().
 */
function senapi_ui_preprocess_menu_local_task(array &$variables) {
  //$variables['element']['#link']['url']->setOption('attributes', ['class'=>'rounded']);
}
